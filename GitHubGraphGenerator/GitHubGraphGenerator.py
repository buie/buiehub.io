'''
@author Andrew Buie
'''

from StringIO import StringIO
import pycurl

username = 'buie'

url = 'https://github.com/users/' + username + '/contributions'

storage = StringIO()
c = pycurl.Curl()
c.setopt(c.URL, url)
c.setopt(c.WRITEFUNCTION, storage.write)
c.perform()
c.close()
content = storage.getvalue()
#print content
file = open("graphout", "w+")
file.write(content)

with open("/var/www/buie.github.io/images/graph", "w+") as fout:
    with open("graphout", "rt") as fin:
        for line in fin:
            if "text" not in line:
                fout.write(line.replace('#eeeeee', '#808080'))
