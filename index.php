<!DOCTYPE HTML>
<html>
	<head>
		<title>Andrew Buie</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.onvisible.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<link rel='shortcut icon' href='favicon.ico' type='image/x-icon'/>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="homepage">
			<div id="header">
					<div class="inner">
						<header>
							<h1><a href="index.html" id="logo">Andrew Buie</a></h1>
							<nav>
							<ul class="actions">
								<li><a href="https://www.linkedin.com/pub/andrew-buie/8a/967/267/" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="http://duke-robotics.com" class="icon fa-gear"><span class="label">Duke Robotics</span></a></li>
								<li><a href="https://github.com/buie" class="icon fa-github "><span class="label">Github</span></a></li>
								<li><a href="mailto:buie.andrew@gmail.com" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
							</ul>
							</nav>
						</header>
						<footer>
							<a href="#banner" class="button circled scrolly">more</a>
						</footer>
					</div>
			</div>

			<section id="banner">
				<header>
					<h2><strong>Welcome!</strong></h2>
					<p>
						Hi! I'm Andrew Buie, a junior studying Electrical and Computer Engineering and Computer Science at Duke. I love to mountain bike, develop software, work with robots, and cheer on the San Francisco Giants. Take a look below at some of the stuff I've been working on recently.
					</p>
				</header>
			</section>

			<section class="carousel">
				<div class="reel">
					<article>
						<a href="https://findyourfreespace.com" class="image featured"><img src="images/freespace-logo.png" alt="" /></a>
						<header>
							<h3><a href="https://findyourfreespace.com">FreeSpace</a></h3>
						</header>
						<p>My team's entry in <a href="http://hackduke.org">HackDuke Fall</a> 2014. We won the <a href="http://colab.duke.edu">Innovation Co-Lab</a> challenge and a grant to continue developing the project. We're currently live at Duke and expanding fast.</p>
					</article>

					<article>
						<a href="http://duke-robotics.com" class="image featured"><img src="images/robotics.png" alt="" /></a>
						<header>
							<h3><a href="http://duke-robotics.com">Duke Robotics Club</a></h3>
						</header>
						<p>I'm the co-president of <a href="http://duke-robotics.com">Duke Robotics Club</a>. We build robots for large competitions. Click the robotics logo above to check out our website and projects.</p>
					</article>

					<article>
						<a href="http://springpop.com" class="image featured"><img src="images/springpop.png" alt="" /></a>
						<header>
							<h3><a href="http://springpop.com">Springpop</a></h3>
						</header>
						<p>A contract web development project I worked on with a friend. We built a targeted social networking platform from scratch for a Philadelphia based startup. Take a look at <a href="http://springpop.com">springpop.com</a></p>
					</article>

					<article>
						<a href="http://entrepreneurship.duke.edu/melissaanddoug/" class="image featured"><img src="images/ie.png" alt="" /></a>
						<header>
							<h3><a href="http://entrepreneurship.duke.edu/melissaanddoug/">Melissa & Doug Entrepreneurs</a></h3>
						</header>
						<p>Selected as a member of M&D Entrepreneurs, a program which provides mentorship to undergraduate entrepreneurs at Duke. As a fellow, I've continued developing <a href="http://findyourfreespace.com">FreeSpace.</a></p>
					</article>

					<article>
						<a href="http://kairossociety.com" class="image featured"><img src="images/kairos.png" alt="" /></a>
						<header>
							<h3><a href="http://kairossociety.com">Kairos Society</a></h3>
						</header>
						<p>I help run the <a href="http://kairossociety.com">Kairos Society</a> as the Regional President of North Carolina. Kairos is an international network of young entrepreneurs building scalable ventures.</p>
					</article>

					<article>
						<a href="https://geekli.st/hackathon/hackduke-s14/project/5338472fb4c56da50848e5ee" class="image featured"><img src="images/hackduke.jpeg" alt="" /></a>
						<header>
							<h3><a href="https://geekli.st/hackathon/hackduke-s14/project/5338472fb4c56da50848e5ee">HackDuke Spring 2014</a></h3>
						</header>
						<p>Won best Accessibility app from Willow Tree Apps in Dukes 24 hour hackathon for creating Pebble, Google Glass, and Android apps that provide live closed captioning of conversations.</p>
					</article>

					<article>
						<a href="http://motion.pratt.duke.edu/" class="image featured"><img src="images/baxter.jpg" alt="" /></a>
						<header>
							<h3><a href="http://motion.pratt.duke.edu/">Intelligent Motion Lab</a></h3>
						</header>
						<p>I researched visual servoing and autonomous control using the baxter robot with Professor Kris Hauser to build a teleoperated nursing robot for infectious disease treatment.</p>
					</article>
				</div>
			</section>

			<div id="footer">
					<div class="row">
						<div class="12u">
								<div class="copyright">
									<?php require_once('images/graph'); ?>
								</div>
						</div>
					</div>
				</div>
			</div>
	</body>
</html>
