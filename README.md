# buie.github.io
Source code for my personal website andrewjbuie.com.

Requires that ```GitHubGraphGenerator.py``` be added to the crontab. This script pulls the GitHub commit map from a specified users account and modifies the SVG. I set the cronjob to run once every minute.
